package co.activeMq.jms.JMS.consumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import co.activeMq.jms.JMS.domain.Email;

@Component
public class Consumer {
	static final Logger log = LoggerFactory.getLogger(Consumer.class);

	private static final String TOPIC_NAME = "mailbox_topic";

	private static final String QUEUE_NAME = "mailbox_queue";

	@JmsListener(destination = TOPIC_NAME, containerFactory = "topicListnerFactory")
	public void receiveTopicMessage(@Payload Email bean) {
		log.info("Messgae received", bean);
	}

	@JmsListener(destination = TOPIC_NAME, containerFactory = "topicListnerFactory")
	public void receiveTopicMessag2e(@Payload Email bean) {
		log.info("Messgae received 2", bean);
	}

	@JmsListener(destination = QUEUE_NAME, containerFactory = "queueListnerFactory")
	public void receiveMessage(Email email) {
		System.out.println("Received <" + email + ">");
	}
}
